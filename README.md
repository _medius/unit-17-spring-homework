# Spring Homework

This is spring exercise web application which sends and receives JSON.
It is a students database. DB scheme is in root folder of project.

- Run application with command line: clean package jetty:run
- Application runs on location http://localhost:1111/university/
(if necessary port and web context can be configured on src/resources/props/applicationWeb.properties).
- REST endpoints is looks as follows (HTTP method) url:
  - get all students
    - (GET) http://localhost:1111/university/students/list/
  - get student by id
    - (GET) http://localhost:1111/university/students/{id}/
  - delete student by id
    - (DELETE) http://localhost:1111/university/students/{id}/
  - update student by id
    - (PUT) http://localhost:1111/university/students/{id}/
  - create new student
    - (POST) http://localhost:1111/university/students/
- In root folder of project there is the postman collection to test endpoints.
- There is two profiles in project:
    - local (default) - Creates embedded database and fill it with test data.
    Test data contains 4 students, 2 groups and 8 exams.
    - prod - Connects to on-disk database postgreSQL and needs to create 
    database called "university". Port, password and username configures 
    on src/resources/props/postgreSQL.properties.