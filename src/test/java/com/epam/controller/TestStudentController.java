package com.epam.controller;

import com.epam.config.SpringConfig;
import com.epam.domain.Student;
import com.epam.service.interfaces.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
@WebAppConfiguration
@ActiveProfiles("local")
public class TestStudentController {

	private static final int TEST_ID = 2;
	private static final int WANTED_NUMBER_OF_INVOCATIONS = 1;

	@Autowired
	private Student testStudent;

	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	@Mock
	private StudentService service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(new StudentController(service)).build();
	}

	@Test
	public void findAll() throws Exception {
		List<Student> studentList = Collections.singletonList(testStudent);

		when(service.findAll()).thenReturn(studentList);

		mockMvc.perform(get("/students/list"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(studentList)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).findAll();
	}

	@Test
	public void findById() throws Exception {
		when(service.findById(TEST_ID)).thenReturn(testStudent);

		mockMvc.perform(get("/students/{id}", TEST_ID))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(testStudent)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
	}

	@Test
	public void deleteById() throws Exception {
		doNothing().when(service).deleteById(TEST_ID);

		mockMvc.perform(delete("/students/{id}", TEST_ID))
				.andDo(print())
				.andExpect(status().isNoContent());

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).deleteById(TEST_ID);
	}

	@Test
	public void create() throws Exception {
		doNothing().when(service).save(testStudent);

		mockMvc.perform(post("/students")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testStudent)))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(testStudent)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).save(testStudent);
	}

	@Test
	public void update() throws Exception {
		when(service.update(testStudent, TEST_ID)).thenReturn(testStudent);

		mockMvc.perform(put("/students/{id}", TEST_ID)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testStudent)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(testStudent)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).update(testStudent, TEST_ID);
	}
}
