package com.epam.service;

import com.epam.config.SpringConfig;
import com.epam.dao.interfaces.DAO;
import com.epam.domain.Student;
import com.epam.service.impl.StudentServiceImpl;
import com.epam.service.interfaces.StudentService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BindException;
import org.springframework.validation.SmartValidator;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
@WebAppConfiguration
@ActiveProfiles("local")
public class TestPositiveStudentService {

	private static final int TEST_ID = 2;
	private static final int WANTED_NUMBER_OF_INVOCATIONS = 1;

	@Autowired
	private SmartValidator validator;

	@Autowired
	private Student testStudent;

	@Mock
	private DAO<Student> dao;

	private StudentService service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		service = new StudentServiceImpl(dao, validator);
	}

	@Test
	public void findAll() {
		List<Student> studentList = Collections.singletonList(testStudent);

		when(dao.findAll()).thenReturn(studentList);

		assertEquals(studentList, service.findAll());

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).findAll();
	}

	@Test
	public void findById() {
		when(dao.findById(TEST_ID)).thenReturn(testStudent);

		assertEquals(testStudent, service.findById(TEST_ID));

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
	}

	@Test
	public void deleteById() {
		when(dao.findById(TEST_ID)).thenReturn(testStudent);

		service.deleteById(TEST_ID);

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).remove(testStudent);
	}

	@Test
	public void save() throws BindException {
		when(dao.save(testStudent)).thenReturn(testStudent);

		service.save(testStudent);

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).save(testStudent);
	}

	@Test
	public void update() throws BindException {
		when(dao.findById(TEST_ID)).thenReturn(testStudent);
		when(dao.update(testStudent)).thenReturn(testStudent);

		assertEquals(testStudent, service.update(testStudent, TEST_ID));

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).update(testStudent);
	}
}
