package com.epam.service;

import com.epam.config.SpringConfig;
import com.epam.dao.interfaces.DAO;
import com.epam.domain.Student;
import com.epam.error.StudentNotFoundException;
import com.epam.service.impl.StudentServiceImpl;
import com.epam.service.interfaces.StudentService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BindException;
import org.springframework.validation.SmartValidator;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
@WebAppConfiguration
@ActiveProfiles("local")
public class TestNegativeStudentService {

	private static final int TEST_ID = 2;
	private static final int WANTED_NUMBER_OF_INVOCATIONS = 1;

	private String originalFirstName;

	@Autowired
	private SmartValidator validator;

	@Autowired
	private Student testStudent;

	@Mock
	private DAO<Student> dao;

	private StudentService service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		service = new StudentServiceImpl(dao, validator);
		originalFirstName = testStudent.getFirstName();
	}

	@After
	public void tearDown() {
		testStudent.setFirstName(originalFirstName);
	}

	@Test(expected = StudentNotFoundException.class)
	public void notFoundById() {
		when(dao.findById(TEST_ID)).thenReturn(null);

		assertEquals(testStudent, service.findById(TEST_ID));

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(null);
	}

	@Test(expected = StudentNotFoundException.class)
	public void deleteNotFoundById() {
		when(dao.findById(TEST_ID)).thenReturn(null);

		service.deleteById(TEST_ID);

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
	}

	@Test(expected = StudentNotFoundException.class)
	public void updateNotFound() throws BindException {
		when(dao.findById(TEST_ID)).thenReturn(null);

		assertEquals(testStudent, service.update(testStudent, TEST_ID));

		verify(dao, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
	}

	@Test(expected = BindException.class)
	public void updateNotValidData() throws BindException {
		testStudent.setFirstName("");
		when(dao.findById(TEST_ID)).thenReturn(testStudent);

		assertEquals(testStudent, service.update(testStudent, TEST_ID));
	}

	@Test(expected = BindException.class)
	public void saveNotValidData() throws BindException {
		testStudent.setFirstName("");

		service.save(testStudent);
	}
}
