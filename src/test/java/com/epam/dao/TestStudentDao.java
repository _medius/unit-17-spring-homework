package com.epam.dao;

import com.epam.config.SpringConfig;
import com.epam.dao.interfaces.DAO;
import com.epam.domain.Student;
import com.epam.util.ListMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
@WebAppConfiguration
@ActiveProfiles("local")
public class TestStudentDao {

	private static final int TEST_FIND_ID = 1;
	private static final int TEST_DELETE_ID = 2;
	private static final int TEST_UPDATE_ID = 3;
	private static final String FIND_ALL = "FROM Student";

	@Autowired
	private Student testStudent;

	@Autowired
	private DAO<Student> dao;

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void findAll() {
		ListMatcher<Student> listMatcher = new ListMatcher<>();

		assertTrue(listMatcher
				.isListsMatch(entityManager.createQuery(FIND_ALL, Student.class).getResultList(), dao.findAll()));
	}

	@Test
	public void findById() {
		assertThat(dao.findById(TEST_FIND_ID)).isEqualTo(entityManager.find(Student.class, TEST_FIND_ID));
	}

	@Test
	public void save() {
		dao.save(testStudent);
		assertEquals(testStudent, entityManager.find(Student.class, testStudent.getId()));
	}

	@Test
	public void update() {
		Student studentToUpdate = entityManager.find(Student.class, TEST_UPDATE_ID);

		studentToUpdate.setFirstName(testStudent.getFirstName());
		studentToUpdate.setLastName(testStudent.getLastName());
		studentToUpdate.setGroup(testStudent.getGroup());
		studentToUpdate.setAddress(testStudent.getAddress());
		studentToUpdate.setExams(testStudent.getExams());

		dao.update(studentToUpdate);
		assertEquals(testStudent, entityManager.find(Student.class, TEST_UPDATE_ID));
	}

	@Test
	public void remove() {
		dao.remove(entityManager.find(Student.class, TEST_DELETE_ID));
		assertNull(entityManager.find(Student.class, TEST_DELETE_ID));
	}
}
