package com.epam.integration;

import com.epam.config.SpringConfig;
import com.epam.dao.interfaces.DAO;
import com.epam.domain.Student;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
@ActiveProfiles("local")
@WebAppConfiguration
public class TestStudentControllerIntegration {

	private static final int ID_TO_NOT_FOUND = 25;
	private static final int FIRST_ELEMENT_OF_LIST = 0;

	private MockMvc mockMvc;
	private Integer testId;
	private String originalFirstName;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private Student testStudent;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private DAO<Student> studentDAO;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.build();
		testId = studentDAO.findAll().get(FIRST_ELEMENT_OF_LIST).getId();
		originalFirstName = testStudent.getFirstName();
	}

	@After
	public void tearDown() {
		testStudent.setFirstName(originalFirstName);
	}

	@Test
	public void testGetById() throws Exception {

		mockMvc.perform(get("/students/{id}", testId))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("id").value(testId));
	}

	@Test
	public void testGetStudentNotFound() throws Exception {
		mockMvc.perform(get("/students/{id}", ID_TO_NOT_FOUND))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void testDeleteStudentNotFound() throws Exception {
		mockMvc.perform(delete("/students/{id}", ID_TO_NOT_FOUND))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void testPutStudentNotFound() throws Exception {
		mockMvc.perform(put("/students/{id}", ID_TO_NOT_FOUND)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsBytes(testStudent)))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void testGetAll() throws Exception {
		List<Student> allStudents = studentDAO.findAll();

		mockMvc.perform(get("/students/list"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(allStudents)));
	}

	@Test
	public void testDelete() throws Exception {
		mockMvc.perform(delete("/students/{id}", testId))
				.andDo(print())
				.andExpect(status().isNoContent());
	}

	@Test
	public void testCreate() throws Exception {
		mockMvc.perform(post("/students")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsBytes(testStudent)))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("firstName", is("testFirstName")))
				.andExpect(jsonPath("lastName", is("testLastName")));

	}

	@Test
	public void testNotCreated() throws Exception {
		testStudent.setFirstName("");
		mockMvc.perform(post("/students")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsBytes(testStudent)))
				.andDo(print())
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testUpdate() throws Exception {
		Student studentFromBase = studentDAO.findById(testId);

		Student testStudent = new Student();
		testStudent.setFirstName("testFirstName");
		testStudent.setLastName("testLastName");
		testStudent.setExams(studentFromBase.getExams());
		testStudent.setAddress(studentFromBase.getAddress());
		testStudent.setGroup(studentFromBase.getGroup());

		mockMvc.perform(put("/students/{id}", testId)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsBytes(testStudent)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("firstName", is("testFirstName")))
				.andExpect(jsonPath("lastName", is("testLastName")));
	}

	@Test
	public void testNotUpdated() throws Exception {
		testStudent.setFirstName("");
		mockMvc.perform(put("/students/{id}", testId)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(testStudent)))
				.andDo(print())
				.andExpect(status().isBadRequest());
	}
}
