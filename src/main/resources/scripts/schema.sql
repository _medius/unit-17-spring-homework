CREATE TABLE addresses (
    id INT NOT NULL AUTO_INCREMENT,
    apartment INT,
    building INT,
    city VARCHAR(255),
    street VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE exams (
    id INT NOT NULL AUTO_INCREMENT,
    date DATE,
    grade INT,
    subject VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE groups (
    id INT NOT NULL AUTO_INCREMENT,
    faculty VARCHAR(255),
    number INT,
    PRIMARY KEY (id)
);

CREATE TABLE students (
    id INT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    address_id INT,
    group_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (address_id) REFERENCES addresses (id),
    FOREIGN KEY (group_id) REFERENCES groups (id)
);

CREATE TABLE students_exams (
    student_id INT NOT NULL,
    exams_id INT NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students (id),
    FOREIGN KEY (exams_id) REFERENCES exams (id)
);
