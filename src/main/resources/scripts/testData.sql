INSERT INTO groups (faculty, number) VALUES ('TOI', 111);
INSERT INTO groups (faculty, number) VALUES ('ITTS', 222);

INSERT INTO addresses (apartment, building, city, street) VALUES (15, 100, 'New York', 'Main Street');
INSERT INTO addresses (apartment, building, city, street) VALUES (135, 17, 'Samara', 'Sovetskoy Armiy');
INSERT INTO addresses (apartment, building, city, street) VALUES (1, 88, 'Cherepovets', 'Lenina');
INSERT INTO addresses (apartment, building, city, street) VALUES (78, 1, 'Sidney', 'Spider Street');

INSERT INTO exams (date, grade, subject) VALUES ('2019-03-12', 4, 'Math');
INSERT INTO exams (date, grade, subject) VALUES ('2019-02-23', 2, 'Language');
INSERT INTO exams (date, grade, subject) VALUES ('2019-01-09', 2, 'Math');
INSERT INTO exams (date, grade, subject) VALUES ('2019-04-18', 5, 'Language');
INSERT INTO exams (date, grade, subject) VALUES ('2019-05-23', 5, 'Physics');
INSERT INTO exams (date, grade, subject) VALUES ('2019-08-11', 4, 'Music');
INSERT INTO exams (date, grade, subject) VALUES ('2019-07-01', 5, 'Physics');
INSERT INTO exams (date, grade, subject) VALUES ('2019-12-30', 1, 'Music');

INSERT INTO students (firstName, lastName, group_id, address_id) VALUES ('Luke', 'Skywalker', 1, 1);
INSERT INTO students (firstName, lastName, group_id, address_id) VALUES ('Mace', 'Windu', 1, 2);
INSERT INTO students (firstName, lastName, group_id, address_id) VALUES ('Obi-Wan', 'Kenobi', 2, 3);
INSERT INTO students (firstName, lastName, group_id, address_id) VALUES ('Leia', 'Organa', 2, 4);

INSERT INTO students_exams (student_id, exams_id) VALUES (1, 1);
INSERT INTO students_exams (student_id, exams_id) VALUES (1, 2);
INSERT INTO students_exams (student_id, exams_id) VALUES (2, 3);
INSERT INTO students_exams (student_id, exams_id) VALUES (2, 4);
INSERT INTO students_exams (student_id, exams_id) VALUES (3, 5);
INSERT INTO students_exams (student_id, exams_id) VALUES (3, 6);
INSERT INTO students_exams (student_id, exams_id) VALUES (4, 7);
INSERT INTO students_exams (student_id, exams_id) VALUES (4, 8);