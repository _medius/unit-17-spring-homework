package com.epam.dao.impl;

import com.epam.dao.interfaces.DAO;
import com.epam.domain.Student;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class StudentDAOimpl implements DAO<Student> {

	private static final String ALL_STUDENTS = "SELECT s FROM Student s";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = true)
	public Student findById(Integer id) {
		return entityManager.find(Student.class, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Student> findAll() {
		return entityManager.createQuery(ALL_STUDENTS, Student.class).getResultList();
	}

	@Override
	@Transactional
	public Student save(Student student) {
		if (student.getId() == null) {
			entityManager.persist(student);
		} else {
			student = entityManager.merge(student);
		}

		if(student.getGroup().getId() == null) {
			entityManager.persist(student.getGroup());
		} else {
			student.setGroup(entityManager.merge(student.getGroup()));
		}

		return student;
	}

	@Override
	@Transactional
	public Student update(Student student) {
		return entityManager.merge(student);
	}

	@Override
	@Transactional
	public void remove(Student student) {
		entityManager.remove(entityManager.merge(student));
	}
}
