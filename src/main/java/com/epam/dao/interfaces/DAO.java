package com.epam.dao.interfaces;

import java.util.List;

public interface DAO<T> {

	T save(T o);
	T update(T o);
	T findById(Integer id);
	List<T> findAll();
	void remove(T o);

}
