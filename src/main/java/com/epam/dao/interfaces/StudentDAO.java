package com.epam.dao.interfaces;

import com.epam.domain.Student;

public interface StudentDAO extends DAO<Student> {
	void removeById(Integer id);
}
