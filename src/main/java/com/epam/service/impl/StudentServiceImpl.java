package com.epam.service.impl;

import com.epam.dao.interfaces.DAO;
import com.epam.domain.Student;
import com.epam.error.StudentNotFoundException;
import com.epam.service.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

	private final DAO<Student> studentDAO;
	private final SmartValidator validator;

	public StudentServiceImpl(DAO<Student> studentDAO, SmartValidator validator) {
		this.studentDAO = studentDAO;
		this.validator = validator;
	}

	@Override
	public List<Student> findAll() {
		return studentDAO.findAll();
	}

	@Override
	public Student findById(Integer id) {
		Student student = studentDAO.findById(id);
		if (student == null) {
			throw new StudentNotFoundException(id);
		}
		return student;
	}

	@Override
	public void deleteById(Integer id) {
		Student studentToRemove = studentDAO.findById(id);
		if (studentToRemove == null) {
			throw new StudentNotFoundException(id);
		}
		studentDAO.remove(studentToRemove);
	}

	@Override
	public void save(Student student) throws BindException {
		validateStudent(student);

		studentDAO.save(student);
	}

	@Override
	public Student update(Student student, Integer id) throws BindException {
		validateStudent(student);

		Student studentToUpdate = studentDAO.findById(id);

		if (studentToUpdate == null) {
			throw new StudentNotFoundException(id);
		}

		studentToUpdate.setFirstName(student.getFirstName());
		studentToUpdate.setLastName(student.getLastName());
		studentToUpdate.setGroup(student.getGroup());
		studentToUpdate.setAddress(student.getAddress());
		studentToUpdate.setExams(student.getExams());

		return studentDAO.update(studentToUpdate);
	}

	private void validateStudent(Student student) throws BindException {
		BindingResult result = new BeanPropertyBindingResult(student, "student");
		validator.validate(student, result);

		if (result.hasErrors()) {
			throw new BindException(result);
		}
	}
}
