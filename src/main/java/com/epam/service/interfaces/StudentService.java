package com.epam.service.interfaces;

import com.epam.domain.Student;
import org.springframework.validation.BindException;

import java.util.List;

public interface StudentService {

	List<Student> findAll();
	Student findById(Integer id);
	void deleteById(Integer id);
	void save(Student student) throws BindException;
	Student update (Student student, Integer id) throws BindException;
}
