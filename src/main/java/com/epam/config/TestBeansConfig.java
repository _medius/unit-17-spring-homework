package com.epam.config;

import com.epam.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.util.Collections;

@Configuration
@Profile("local")
public class TestBeansConfig {

	@Bean
	public Exam testExam() {
		Exam exam = new Exam();
		exam.setSubject("testSubject");
		exam.setGrade(5);
		exam.setDate(LocalDate.now());
		return exam;
	}

	@Bean
	public Address testAddress() {
		Address address = new Address();
		address.setCity("testCity");
		address.setStreet("testStreet");
		address.setBuilding(100500);
		address.setApartment(100500);
		return address;
	}

	@Bean
	public Group testGroup() {
		Group group = new Group();
		group.setNumber(777);
		group.setFaculty("testFaculty");
		return group;
	}

	@Bean
	@Autowired
	public Student testStudent(Exam testExam, Address testAddress, Group testGroup) {
		Student student = new Student();
		student.setFirstName("testFirstName");
		student.setLastName("testLastName");
		student.setGroup(testGroup);
		student.setAddress(testAddress);
		student.setExams(Collections.singletonList(testExam));
		return student;
	}
}
