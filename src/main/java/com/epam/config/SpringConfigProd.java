package com.epam.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

import java.util.Properties;

@Configuration
@Profile("prod")
@PropertySource(value = {"classpath:props/postgreSQL.properties"},
		ignoreResourceNotFound = true)
public class SpringConfigProd {

	@Value("${psql_driver_class}")
	private String driverClass;

	@Value("${psql_url}")
	private String url;

	@Value("${psql_username}")
	private String username;

	@Value("${psql_password}")
	private String password;

	@Value("${psql_show_sql}")
	private String showSql;

	@Value("${psql_format_sql}")
	private String formatSql;

	@Value("${psql_dialect}")
	private String dialect;

	@Value("${psql_hbm2ddl}")
	private String hbm2ddl;

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public LocalEntityManagerFactoryBean entityManagerFactory() {
		LocalEntityManagerFactoryBean localEntityManagerFactoryBean = new LocalEntityManagerFactoryBean();
		localEntityManagerFactoryBean.setPersistenceUnitName("HibernateUnit");

		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.connection.driver_class", driverClass);
		jpaProperties.put("hibernate.connection.url", url);
		jpaProperties.put("hibernate.connection.username", username);
		jpaProperties.put("hibernate.connection.password", password);
		jpaProperties.put("hibernate.show_sql", showSql);
		jpaProperties.put("hibernate.format_sql", formatSql);
		jpaProperties.put("hibernate.dialect", dialect);
		jpaProperties.put("hibernate.hbm2ddl.auto", hbm2ddl);
		localEntityManagerFactoryBean.setJpaProperties(jpaProperties);

		return localEntityManagerFactoryBean;
	}
}
