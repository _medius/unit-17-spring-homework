package com.epam.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@Profile("local")
@PropertySource(value = {"classpath:props/H2db.properties"},
		ignoreResourceNotFound = true)
@Import({TestBeansConfig.class})
public class SpringConfigLocal {

	@Value("${h2_show_sql}")
	private String showSql;

	@Value("${h2_format_sql}")
	private String formatSql;

	@Value("${h2_dialect}")
	private String dialect;

	@Value("${h2_hbm2ddl}")
	private String hbm2ddl;


	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public DataSource dataSource() {
		return new EmbeddedDatabaseBuilder()
				.setType(EmbeddedDatabaseType.H2)
				.setScriptEncoding("UTF-8")
				.addScript("scripts/schema.sql")
				.addScript("scripts/testData.sql")
				.build();
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean localEntityManagerFactoryBean =
				new LocalContainerEntityManagerFactoryBean();
		localEntityManagerFactoryBean.setPersistenceUnitName("HibernateUnit");
		localEntityManagerFactoryBean.setDataSource(dataSource);
		localEntityManagerFactoryBean.getDataSource();

		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.show_sql", showSql);
		jpaProperties.put("hibernate.format_sql", formatSql);
		jpaProperties.put("hibernate.dialect", dialect);
		jpaProperties.put("hibernate.hbm2ddl.auto", hbm2ddl);
		localEntityManagerFactoryBean.setJpaProperties(jpaProperties);

		return localEntityManagerFactoryBean;
	}
}
