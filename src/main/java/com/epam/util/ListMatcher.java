package com.epam.util;

import java.util.List;

public class ListMatcher<T> {

	public boolean isListsMatch(List<T> firstList, List<T> secondList) {
		if (firstList.size() != secondList.size()) {
			return false;
		}

		if (firstList.size() == 0) {
			return true;
		}

		for (T t : firstList) {
			if (!secondList.contains(t)) {
				return false;
			}
		}

		return true;
	}
}