package com.epam.error;

public class StudentNotFoundException extends RuntimeException {

	public StudentNotFoundException(String message) {
		super(message);
	}

	public StudentNotFoundException(Integer id) {
		super("Student not found by id " + id);
	}
}
