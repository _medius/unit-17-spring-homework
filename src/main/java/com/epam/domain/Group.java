package com.epam.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "groups")
public class Group {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private int number;

	@Column
	private String faculty;

	public Group() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	@Override
	public String toString() {
		return "Group{" +
				"number=" + number +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Group)) return false;
		Group group = (Group) o;
		return getNumber() == group.getNumber() &&
				getFaculty().equals(group.getFaculty());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getNumber(), getFaculty());
	}
}
