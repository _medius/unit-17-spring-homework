package com.epam.domain;

import com.epam.util.ListMatcher;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "students")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	@NotEmpty
	private String firstName;

	@Column
	@NotEmpty
	private String lastName;

	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "group_id")
	private Group group;

	@OneToMany(fetch = FetchType.EAGER,
			cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private List<Exam> exams;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	@NotNull
	private Address address;

	public Student() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student{" +
				"id=" + id +
				",\n firstName='" + firstName + '\'' +
				",\n lastName='" + lastName + '\'' +
				",\n group=" + group +
				",\n exams=" + exams +
				",\n address=" + address +
				"} \n \n";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Student)) return false;
		Student student = (Student) o;
		ListMatcher<Exam> listMatcher = new ListMatcher<>();
		return getFirstName().equals(student.getFirstName()) &&
				getLastName().equals(student.getLastName()) &&
				getGroup().equals(student.getGroup()) &&
				listMatcher.isListsMatch(getExams(), student.getExams()) &&
				getAddress().equals(student.getAddress());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getFirstName(), getLastName(), getGroup(), getExams(), getAddress());
	}
}
