package com.epam.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "exams")
public class Exam {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String subject;

	@Column
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
	private LocalDate date;

	@Column
	private int grade;

	public Exam() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Exam{" +
				"id=" + id +
				", subject='" + subject + '\'' +
				", date=" + date +
				", grade=" + grade +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Exam)) return false;
		Exam exam = (Exam) o;
		return getGrade() == exam.getGrade() &&
				getSubject().equals(exam.getSubject()) &&
				getDate().equals(exam.getDate());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getSubject(), getDate(), getGrade());
	}
}
