package com.epam.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "addresses")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String city;

	@Column
	private String street;

	@Column
	private int building;

	@Column
	private int apartment;

	public Address() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getBuilding() {
		return building;
	}

	public void setBuilding(int building) {
		this.building = building;
	}

	public int getApartment() {
		return apartment;
	}

	public void setApartment(int apartment) {
		this.apartment = apartment;
	}

	@Override
	public String toString() {
		return "Address{" +
				"id=" + id +
				", city='" + city + '\'' +
				", street='" + street + '\'' +
				", building=" + building +
				", apartment=" + apartment +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Address)) return false;
		Address address = (Address) o;
		return getBuilding() == address.getBuilding() &&
				getApartment() == address.getApartment() &&
				getCity().equals(address.getCity()) &&
				getStreet().equals(address.getStreet());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getCity(), getStreet(), getBuilding(), getApartment());
	}
}
