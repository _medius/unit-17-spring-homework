package com.epam.controller;

import com.epam.domain.Student;
import com.epam.service.interfaces.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

	private final StudentService studentService;

	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}

	@GetMapping(value = "/list")
	public List<Student> findAll() {
		return studentService.findAll();
	}

	@GetMapping(value = "/{id}")
	public Student findById(@PathVariable("id") Integer id) {
		return studentService.findById(id);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteStudentById(@PathVariable("id") Integer id) {
		studentService.deleteById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Student createStudent(@RequestBody Student student,
	                             HttpServletResponse response) throws BindException {
		studentService.save(student);

		response.setHeader("Location", "/students/" + student.getId() + "/");
		return student;
	}

	@PutMapping(value = "/{id}")
	public Student updateStudent(@PathVariable("id") Integer id, @RequestBody Student student) throws BindException {
		return studentService.update(student, id);
	}
}